import random
import re
from copy import copy

verbose = True

def condPrint(message):
    if verbose:
        print(message)

# Return an array of member names
def getMembers():
    with open('members.txt', 'r') as f:
        x = f.readlines()
        if len(x) == 0:
            return []
        return x[0].rstrip().split(',')

# Return an array of partnership tuples
def getPartners():
    with open('partners.txt', 'r') as f:
        x = f.readlines()
        y = []
        for line in x:
            line = line.rstrip()
            partners = line.split(',')
            y.append((partners[0], partners[1]))
        return y

# Write a new member to the member file
def writeMember(name):
    with open('members.txt', 'r+') as f:
        file_data = f.read()
        f.seek(0, 0)
        if len(file_data) == 0:
            f.write(name + '\n')
        else:
            f.write(file_data.rstrip() + ',' + name + '\n')

# Write a new partnership to the partner file
def writePartners(name1, name2):
    with open('partners.txt', 'a') as f:
        f.write(name1+','+name2+'\n')

# Returns a random member who is not assigned as a giver
def chooseGiver(members, givers):
    membersCopy = copy(members)

    for giver in givers:
       membersCopy.remove(giver)

    if len(membersCopy) == 0:
        condPrint('No givers left!')
        return None
    return random.choice(membersCopy)

# Returns a random member who is not assigned as a recipient,
# not the assigned giver, and not the givers partner
def chooseRecipient(members, recipients, partners, giver):
    errorMsg    = 'No recipients left!'
    membersCopy = copy(members)

    for recipient in recipients:
       membersCopy.remove(recipient)

    if giver in membersCopy:
       errorMsg = 'Rule violation: ' + giver + ' cannot receive own gift!'
       membersCopy.remove(giver)

    for partnership in partners:
       errorMsg = 'Rule violation: ' + giver + ' cannot give partner a gift!'
       if giver in partnership:
           for member in partnership:
               if member in membersCopy:
                   membersCopy.remove(member)

    if len(membersCopy) == 0:
        condPrint(errorMsg)
        return None
    return random.choice(membersCopy)

# Register a new member with a valid name
def registerName(name):
    #name = re.sub('[\'"]', '', name)
    members = getMembers()
    if not re.match(r'^[a-zA-Z.0-9 ]*$', name):
        print('Invalid characters in name.')
        exit()
    if name in members:
        print(name + ' is already a member!')
    else:
        print(name + ' added to registry!')
        writeMember(name)

# Assign two existing members to a partnership
def registerPartners(name1, name2):
    members  = getMembers()
    partners = getPartners()
    for partnership in partners:
        if name1 in partnership:
            print(name1 + ' is already in a partnership!')
            exit()
        if name2 in partnership:
            print(name2 + ' is already in a partnership!')
            exit()
    if name1 not in members:
        registerName(name1)
    if name2 not in members:
        registerName(name2)
    print(name1 + ' and ' + name2 + ' added as partners!')
    writePartners(name1, name2)
