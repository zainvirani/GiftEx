#!/usr/bin/python

# Registration Command Line App
#
# Author: Zain Virani
#
# Assumptions:
# 1. Member names are unique
#
# Usage:
# (register 1 member)
# ./register.py "First Last"
#
# (register 2 partners)
# ./register.py "Partner 1" "Partner 2"
#
# (list members and partners and print instructions)
# ./register.py
##

import sys
import lib
import pprint

if __name__ == '__main__':
    members  = lib.getMembers()
    partners = lib.getPartners()
    pp       = pprint.PrettyPrinter(indent=4)

    if len(sys.argv) == 1 or len(sys.argv) > 3:
        print('Members:')
        pp.pprint(members)
        print('Partners:')
        pp.pprint(partners)
        print(
               'To register a member run `python register.py NAME`\n'
               'To register a partnership run `python register.py NAME_1 NAME_2`\n'
               'To see this message again run `python register.py`'
        )
    elif len(sys.argv) == 2:
        lib.registerName(sys.argv[1])
    elif len(sys.argv) == 3:
        lib.registerPartners(sys.argv[1], sys.argv[2])
