# Gift Exchange

This command line tool can be used to register members and match them for a gift exchange.

## Usage

1. Clone this repo
2. Ensure `giftExchange.py` and `register.py` are executable by the user
3. To register a member run `./register.py NAME`
4. To register a partnership run `./register.py NAME_1 NAME_2` (if they are not already members, they will be added)
5. Note that to register members with a first and last name, the name must be surrounded by quotes as "First Last"
6. To run the gift exchange program run `./giftExchange.py`

### Testing

1. Run `./test.py X` where X is some integer
2. The program will simulate X individual trials
3. The program will output the average number of iterations for success, the total iterations, and the # of invalid results
4. If you run `./test.py X -v` the program will also output all match lists and error messages

Author: Zain Virani

Python Version: 2.7.1+
