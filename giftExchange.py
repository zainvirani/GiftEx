#!/usr/bin/python

# Gift Exchange Command Line App
#
# Author: Zain Virani
#
# Rules:
# 1. Members cannot draw their own name
# 2. Members cannot draw the name of their partner
#
# Assumptions:
# 1. Member names are unique
# 2. Members must give and receive 1 gift
#
# Usage:
# ./giftExchange.py will run the gift exchange program one time
#
# Output:
# A list of matches that do not violate the rules
##

import lib
import sys

verbose = True;

def condPrint(message):
    if verbose:
        print(message)

def giftExchange():
    lib.verbose = verbose
    iteration = 1
    condPrint('Starting gift exchange!')
    condPrint('Iteration ' + str(iteration))
    members  = lib.getMembers()
    partners = lib.getPartners()

    if (len(members) < 2 or
       (len(members) < 4 and len(partners) > 0)):
        print('Please add more members or remove partners.')
        exit()

    givers, recipients, assignments = [], [], []

    while len(set(givers).intersection(members)) != len(members):
        # Choose a giver who isn't already a giver
        giver = lib.chooseGiver(members, givers)
        
        # Choose a recipient who isn't already receiving a gift, isn't the giver,
        # and isn't the giver's partner
        recipient = lib.chooseRecipient(members, recipients, partners, giver)

        if giver is None or recipient is None:
            givers, recipients, assignments = [], [], []
            iteration += 1
            condPrint('Iteration ' + str(iteration))
            continue

        givers.append(giver)
        recipients.append(recipient)
        condPrint(giver + ' => ' + recipient)
        assignments.append((giver, recipient))
    condPrint('Completed in ' + str(iteration) + ' iteration(s)!')
    return [ iteration, assignments ]

if __name__ == '__main__':
    giftExchange()
