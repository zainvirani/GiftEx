#!/usr/bin/python

# Gift Exchange Test
#
# Author: Zain Virani
#
# This script will run the gift exchange program repeatedly,
# then validate and summarize results.
#
# Usage:
# ./test.py X will run it X times and display stats
# ./test.py X -v will output all lists and errors
#
# Output:
# Lists of matches that do not violate the rules
# Statistics regarding average iterations and # of errors
##

import giftExchange as GE
import sys
import lib

def assignmentIsValid(assignments, partners):
    for match in assignments:
        if (match[0] == match[1] or
           (match[0], match[1]) in partners or
           (match[1], match[0]) in partners):
            return False
    return True

if __name__ == '__main__':
    if len(sys.argv) > 1:
        GE.verbose = False
        if len(sys.argv) == 3:
            if sys.argv[2] == '-v':
                GE.verbose = True
        try:
            partners = lib.getPartners()
            limit = int(sys.argv[1])
            if limit == 0:
                exit()
            accumulator = 0
            errors = 0
            for i in range(limit):
                results = GE.giftExchange()
                if not assignmentIsValid(results[1], partners):
                    errors += 1
                accumulator += results[0]
            print('Avg Iterations:   ' + str(accumulator/float(limit)))
            print('Total Iterations: ' + str(accumulator))
            print('Num of Errors:    ' + str(errors))
        except TypeError:
            print('Please pass an integer as an argument.')
        except ValueError:
            print('Please pass an integer as an argument.')
    else:
        print('Please pass an integer as an argument.')
